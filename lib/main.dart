import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:atualcanceperu/pages/home_page.dart';
import 'package:atualcanceperu/pages/mapa_page.dart';

import 'package:atualcanceperu/providers/scan_list_provider.dart';
import 'package:atualcanceperu/providers/ui_provider.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:atualcanceperu/inicio.dart';
import 'package:atualcanceperu/scan.dart';
import 'package:atualcanceperu/pages/sitios.dart'; 
import 'package:atualcanceperu/pages/nasca.dart'; 
import 'package:atualcanceperu/pages/mscolores.dart'; 
import 'package:atualcanceperu/pages/listar.dart'; 
import 'package:atualcanceperu/pages/tumi.dart'; 
List<CameraDescription> cameras;

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    cameras = await availableCameras();
  } on CameraException catch (e) {
    print('Error: $e.code\nError Message: $e.message');
  }
  runApp(new MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => new UiProvider() ),
        ChangeNotifierProvider(create: (_) => new ScanListProvider() ),
      ],

      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'AtualcancePeru',
        initialRoute: 'Tumi',
        routes: {
          '/'   : (BuildContext context) => Inicio(cameras),     
          'scan':(BuildContext context) => ScanPage(cameras),    
          'home': (BuildContext context) => HomePage(),
          'mapa': (BuildContext context) => MapaPage(),
          'MP': (BuildContext context) => SitiosPage(), 
          'Nasca': (BuildContext context) => NascaPage(),           
          'MSColores': (BuildContext context) => MSColoresPage(),   
          'Tumi': (BuildContext context) => TumiPage(),   
          'listar': (BuildContext context) => ListarPage(),                  
    

        },
        theme: ThemeData(
          //primaryColor: Colors.deepPurple,
          primaryColor: Colors.red,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Colors.red
          )
        ),
      ),
    );

  }
}
