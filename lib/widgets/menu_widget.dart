import 'package:flutter/material.dart';

import 'package:atualcanceperu/pages/home_page.dart';



class MenuWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/logo4.png'),
                  //fit: BoxFit.cover
                )
              ),
            ),

            ListTile(
              leading: Icon( Icons.home, color: Colors.red ),
              title: Text('Home'),
              onTap: ()=> Navigator.pushReplacementNamed(context, 'listar' ) ,
            ),

            ListTile(
              leading: Icon( Icons.party_mode, color: Colors.red ),
              title: Text('Scan'),
              onTap: ()=> Navigator.pushReplacementNamed(context, 'home' ) ,
            ),

            ListTile(
              leading: Icon( Icons.pages, color: Colors.red ),
              title: Text('Detection image'),
              onTap: ()=> Navigator.pushReplacementNamed(context, '/' ) ,
            ),

            ListTile(
              leading: Icon( Icons.settings, color: Colors.red ),
              title: Text('Settings'),
              onTap: (){
                // Navigator.pop(context);
              }
            ),
            SizedBox(height:72),
            FadeInImage(
              placeholder: AssetImage('assets/giphy.gif'),
              image: NetworkImage('https://scontent-lim1-1.xx.fbcdn.net/v/t1.0-9/134663841_103811281655206_7103734606854659599_n.jpg?_nc_cat=104&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFKWy0tDvjQplxKyhtwUdRt4XUds5_s_2rhdR2zn-z_auddyVz747h1E_CVQoxXW5t4YJ1GpZEcP-YCbOhDVUwc&_nc_ohc=kaD9Yn6OH6sAX9wbV5g&_nc_ht=scontent-lim1-1.xx&oh=c8324cebbc9546908c296b1462cb91a4&oe=60301395'),
              fadeInDuration: Duration(milliseconds:200),
              ),

          ],
        ),
      ),
    );
  }
}
