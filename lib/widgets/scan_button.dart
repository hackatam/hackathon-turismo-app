import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:atualcanceperu/providers/scan_list_provider.dart';
import 'package:atualcanceperu/utils/utils.dart';

class ScanButton extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 0,
      child: Icon( Icons.filter_center_focus ),
      onPressed: () async {

        String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode('#EF3D49', 'Cancelar', false, ScanMode.QR );//#3D8BEF
        // final barcodeScanRes = 'https://fernando-herrera.com';
        // final barcodeScanRes = 'geo:45.287135,-75.920226';

        if ( barcodeScanRes == '-1' ) {
          return;
        }

        final scanListProvider = Provider.of<ScanListProvider>(context, listen: false);
        
        final nuevoScan = await scanListProvider.nuevoScan(barcodeScanRes);

        launchURL(context, nuevoScan);
      }
    );
  }
}