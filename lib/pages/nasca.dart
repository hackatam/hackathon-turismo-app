import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:atualcanceperu/widgets/menu_widget.dart';

class NascaPage extends StatefulWidget {


  @override
  NascaPageState createState() => NascaPageState();
}

class NascaPageState extends State<NascaPage> {
    
  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  final itemHotel=['Hotel1','Hotel2','Hotel3'];
  String _model = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
        title: Text('Líneas de Nasca'),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
          body: SingleChildScrollView(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeInImage(
                      height: 245,
                      placeholder: AssetImage('assets/giphy.gif'),
                      image: NetworkImage('https://www.incasjourneyperu.com/wp-content/uploads/2019/03/lineas-de-nazca-incas-journey.jpg'),
                      fadeInDuration: Duration(milliseconds:200),
                      fit: BoxFit.cover,

                      ),

                      _crearTitulo(),
                      
                      _crearTexto(),
                     SizedBox(height:15),
                     Text('Conoce:',style: TextStyle( fontSize: 20.0,color: Colors.deepPurple )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _hotelTarjeta(), 
                       _artesaniaTarjeta(), 
                       _restauranteTarjeta(),
                        SizedBox(height:10),                                               
                       
                       ]

 

                     ), 
                     _listaTarjeta(),
                     _vuelosTarjeta(),
                     _extraTarjeta(),
                   /*  
                    SizedBox(height:10),
                     Text('Artesanías:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(),                                               
                       ]
                      ),
                      SizedBox(height:10),
                     Text('Restaurantes:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(),                                               
                       ]
                      ),*/
                  ],
                ),
            )


      );
  }

Widget _crearTitulo() {
    
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
        child: Row(
          children: <Widget>[

            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Líneas de Nasca', style: estiloTitulo ),
                  SizedBox( height: 7.0 ),
                  Text('Lugar Histórico del Perú', style: estiloSubTitulo ),
                ],
              ),
            ),

            Icon( Icons.star, color: Colors.red, size: 30.0 ),
            Text('41', style: TextStyle( fontSize: 20.0 ) )

          ],
        ),
      ),
    );
  }
 Widget _crearTexto() {

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric( horizontal: 40.0 ),
        child: Text(
          'Las Líneas de Nazca, en el sur de Perú, son un grupo de geoglifos precolombinos que se extienden en las arenas del desierto. Abarcan un área de casi 1,000 kilómetros cuadrados y hay alrededor de 300 figuras distintas, que incluyen animales y plantas. Están compuestos por más de 10,000 líneas, algunas de estas de 30 metros de ancho y más de 9 kilómetros de largo. Es más fácil ver las figuras desde el aire o desde montañas cercanas.',
          textAlign: TextAlign.justify,
        ),
      ),
    );

  }


  Widget _crearActoresPageView( ) {

    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        pageSnapping: false,
        controller: PageController(
          viewportFraction: 0.3,
          initialPage: 1
        ),
        itemCount: 3,
        itemBuilder: (context, i) =>_hotelTarjeta(),
      ),
    );

  }

  Widget _hotelTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://media-cdn.tripadvisor.com/media/photo-s/13/1d/f0/60/hotel-cocos-inn.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Hoteles',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
  Widget _artesaniaTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://i.pinimg.com/originals/d0/41/c0/d041c0112836e0784531c1946a79e144.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Artesanías',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _restauranteTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIdujHFrvv7p4QuRdaYSHaI1iVJZSQvI7p-w&usqp=CAU'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Restaurantes',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _listaTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Preguntas Frecuentes', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.folder_open,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
 Widget _vuelosTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Vuelos', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.airplanemode_on,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
Widget _extraTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Recomendaciones', style: TextStyle(color: Colors.deepPurple ) ),
        leading: Icon(Icons.wysiwyg,color: Colors.deepPurple)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }


}
