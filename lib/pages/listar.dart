import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:atualcanceperu/widgets/menu_widget.dart';
import 'package:animate_do/animate_do.dart';

class ListarPage extends StatefulWidget {


  @override
  ListarPageState createState() => ListarPageState();
}

class ListarPageState extends State<ListarPage> {
    
  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  final itemHotel=['Hotel1','Hotel2','Hotel3'];
  String _model = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
        title: Text('Sitios Turísticos'),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
          body: SingleChildScrollView(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 20,),






Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
         children:<Widget>[
                      SizedBox(width:1),
                      Column(
                        children: [
                          
                          Container(
                          width: 75.0,
                          height: 75.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ), 
                          child:IconButton(
                            iconSize : 50.0,
                            icon: Icon(Icons.home_work,color: Colors.white),
                          
                          )   
                        ),
                        SizedBox(height:4),
                        Text('Hoteles')
                        ],
                      ),
                       Column(
                         children: [
                         Container(
                          width: 75.0,
                          height: 75.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                         borderRadius: BorderRadius.all(Radius.circular(50)),
                          ), 
                          child:IconButton(
                            iconSize : 50.0,
                            icon: Icon(Icons.adb_sharp,color: Colors.white),
                          
                          )   
                      ),
                       SizedBox(height:4),
                        Text('Artesanías')
                      ],
                       ),
                       
                      Column(
                        children: [
                           Container(
                          
                          width: 75.0,
                          height: 75.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ), 
                          child:IconButton(
                            iconSize : 50.0,
                            icon: Icon(Icons.food_bank,color: Colors.white),
                          
                          )   
                        ),
                         SizedBox(height:4),
                        Text('Restaurantes')
                        ],
                      ),
                      Column(
                        children: [
                           Container(
                          width: 75.0,
                          height: 75.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ), 
                          child:IconButton(
                            iconSize : 50.0,
                            icon: Icon(Icons.emoji_objects,color: Colors.white),
                          
                          )   
                        ),
                        SizedBox(height:4),
                        Text('Planes')
                        ],
                      ),
                      SizedBox(width:1),
         ]
),








                     SizedBox(height:10),

                     
                     _listaTarjeta(),
                     _vuelosTarjeta(),
                     _extraTarjeta(),
                     _extraTarjeta2( ),
                     _extraTarjeta3( ),
                     _extraTarjeta4( ),
                     _extraTarjeta5(),
                                          _listaTarjeta(),
                     _vuelosTarjeta(),
                     _extraTarjeta(),
                     _extraTarjeta2( ),
                     _extraTarjeta3( ),
                     _extraTarjeta4( ),
                     _extraTarjeta5(),


                   /*  
                    SizedBox(height:10),
                     Text('Artesanías:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(),                                               
                       ]
                      ),
                      SizedBox(height:10),
                     Text('Restaurantes:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(),                                               
                       ]
                      ),*/
                  ],
                ),
            )


      );
  }

    Widget _listaTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
            delay: Duration(milliseconds:100),
            child: ListTile(
        title: Text( 'Machu Picchu', style: TextStyle(color:Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Before_Machu_Picchu.jpg/375px-Before_Machu_Picchu.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            Navigator.pushReplacementNamed(context, 'MP' ) ;
       },
      ),
          ),


        
        ]
            ),
        );
    }
 Widget _vuelosTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
          delay: Duration(milliseconds:200),
          child: ListTile(
        title: Text( 'Montaña Siete Colores', style: TextStyle(color:Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://wetravelperu.com/wp-content/uploads/2019/06/vinicunca_1423x950_001-960x1149.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {
            Navigator.pushReplacementNamed(context, 'MSColores' ) ;
            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }
Widget _extraTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
            delay: Duration(milliseconds:300),
            child: ListTile(
        title: Text( 'El barrio de San Blas', style: TextStyle(color: Colors.deepPurple ) ),
       subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://3.bp.blogspot.com/-5pkPv3ScR0I/WlZhZAmP7zI/AAAAAAAAJmY/z6F1ZQlaVk4WOrYdHecpiON4ExBh4eJqACLcBGAs/s400/foto000000000000001208.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }

Widget _extraTarjeta2( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
            delay: Duration(milliseconds:400),
                      child: ListTile(
        title: Text( 'Sacsayhuaman', style: TextStyle(color: Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://portal.andina.pe/EDPfotografia3/Thumbnail/2018/07/11/000517589W.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }
  Widget _extraTarjeta3( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
             delay: Duration(milliseconds:500),
                      child: ListTile(
        title: Text( 'Ollantaytambo', style: TextStyle(color: Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5GvN57V2KdUIA-C6iFhmzf2mjMmM_YLUgQw&usqp=CAU') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }

    Widget _extraTarjeta4( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
             delay: Duration(milliseconds:600),
                      child: ListTile(
        title: Text( 'Choquequirao', style: TextStyle(color: Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://www.turiweb.pe/wp-content/uploads/2020/08/choquequirao-040820.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }

    Widget _extraTarjeta5( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          FadeInLeft(
             delay: Duration(milliseconds:700),
                      child: ListTile(
        title: Text( 'Laguna Humantay', style: TextStyle(color: Colors.deepPurple ) ),
        subtitle: Text( 'Lugar turístico'),
        leading: Container(
            width: 50,
            child:CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage('https://www.paquetesdeviajesperu.com/wp-content/uploads/2019/03/consejos-de-viaje-laguna-humantay-1280x854.jpg') ,
        ),
        ),
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

            //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),
          ),


        
        ]
            ),
        );
    }

}
