import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:atualcanceperu/widgets/menu_widget.dart';

class MSColoresPage extends StatefulWidget {


  @override
  MSColoresPageState createState() => MSColoresPageState();
}

class MSColoresPageState extends State<MSColoresPage> {
    
  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  final itemHotel=['Hotel1','Hotel2','Hotel3'];
  String _model = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
        title: Text('Montaña de Siete Colores'),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
          body: SingleChildScrollView(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeInImage(
                      height: 245,
                      placeholder: AssetImage('assets/giphy.gif'),
                      image: NetworkImage('https://ichef.bbci.co.uk/news/800/cpsprodpb/1021E/production/_101687066_montaa1.jpg'),
                      fadeInDuration: Duration(milliseconds:200),
                      fit: BoxFit.cover,

                      ),

                      _crearTitulo(),
                      
                      _crearTexto(),
                     SizedBox(height:10),
                     Text('Conoce:',style: TextStyle( fontSize: 20.0,color: Colors.deepPurple )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _hotelTarjeta(), 
                       _artesaniaTarjeta(), 
                       _restauranteTarjeta(),
                        SizedBox(height:10),                                               
                       
                       ]

 

                     ), 
                     _listaTarjeta(),
                     _vuelosTarjeta(),
                     _extraTarjeta(),
                   /*  
                    SizedBox(height:10),
                     Text('Artesanías:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(),                                               
                       ]
                      ),
                      SizedBox(height:10),
                     Text('Restaurantes:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(),                                               
                       ]
                      ),*/
                  ],
                ),
            )


      );
  }

Widget _crearTitulo() {
    
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
        child: Row(
          children: <Widget>[

            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Montaña de Siete Colores', style: estiloTitulo ),
                  SizedBox( height: 7.0 ),
                  Text('Lugar Histórico del Perú', style: estiloSubTitulo ),
                ],
              ),
            ),

            Icon( Icons.star, color: Colors.red, size: 30.0 ),
            Text('45', style: TextStyle( fontSize: 20.0 ) )

          ],
        ),
      ),
    );
  }
 Widget _crearTexto() {

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric( horizontal: 40.0 ),
        child: Text(
          'La montaña de los 7 Colores (también llamado Vinicunca o simplemente ‘arcoíris’) es una de las nuevas y mejores atracciones del Perú. Ubicada a más de 100 kilómetros de la ciudad del Cusco, en una cumbre altitudinal situada a 5,200 metros sobre el nivel del mar (m.s.n.m.) Se trata de una formación montañosa teñida de varias tonalidades producto de la compleja combinación de minerales. Las laderas y la cumbre están teñidas de diversas tonos que incluyen el rojo, morado, verde, amarillo, rosado y otras... ',
          textAlign: TextAlign.justify,
        ),
      ),
    );

  }


  Widget _crearActoresPageView( ) {

    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        pageSnapping: false,
        controller: PageController(
          viewportFraction: 0.3,
          initialPage: 1
        ),
        itemCount: 3,
        itemBuilder: (context, i) =>_hotelTarjeta(),
      ),
    );

  }

  Widget _hotelTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://media-cdn.tripadvisor.com/media/photo-s/03/05/e7/fc/unaytambo-hotel.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Hoteles',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
  Widget _artesaniaTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://images.evisos.com.pe/2014/02/11/artesania-ceramica-ccahuana_e3a58da_3.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Artesanías',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _restauranteTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://www.cuscoperu.com/images/CP_festividades_eventos/gastronomia/Chicharon1.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Restaurantes',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _listaTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Preguntas Frecuentes', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.folder_open,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
 Widget _vuelosTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Vuelos', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.airplanemode_on,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
Widget _extraTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Recomendaciones', style: TextStyle(color: Colors.deepPurple ) ),
        leading: Icon(Icons.wysiwyg,color: Colors.deepPurple)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }


}
