import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:atualcanceperu/widgets/menu_widget.dart';

class TumiPage extends StatefulWidget {


  @override
  TumiPageState createState() => TumiPageState();
}

class TumiPageState extends State<TumiPage> {
    
  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 18.0, color: Colors.grey );
  final itemHotel=['Hotel1','Hotel2','Hotel3'];
  String _model = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
        title: Text('Tumi'),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
          body: SingleChildScrollView(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeInImage(
                      height: 245,
                      placeholder: AssetImage('assets/giphy.gif'),
                      image: NetworkImage('https://e.rpp-noticias.io/normal/2017/07/25/035703_451265.jpg'),
                      fadeInDuration: Duration(milliseconds:200),
                      fit: BoxFit.cover,
                      ),

                      _crearTitulo(),
                      _crearTexto(),
                      
                     SizedBox(height:10),
                     Text('Conoce:',style: TextStyle( fontSize: 20.0,color: Colors.deepPurple )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _hotelTarjeta(), 
                       _artesaniaTarjeta(), 
                       _restauranteTarjeta(),
                        SizedBox(height:10),                                               
                       
                       ]

 

                     ), 
                     _listaTarjeta(),
                     //_vuelosTarjeta(),
                     _extraTarjeta(),
                   /*  
                    SizedBox(height:10),
                     Text('Artesanías:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(), 
                       _artesaniaTarjeta(),                                               
                       ]
                      ),
                      SizedBox(height:10),
                     Text('Restaurantes:',style: TextStyle( fontSize: 20.0,color: Colors.blue )),
                     SizedBox(height:10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(), 
                       _restauranteTarjeta(),                                               
                       ]
                      ),*/
                  ],
                ),
            )


      );
  }

Widget _crearTitulo() {
    
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
        child: Row(
          children: <Widget>[

            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Tumi', style: estiloTitulo ),
                  SizedBox( height: 7.0 ),
                  Text('Culturas Moche', style: estiloSubTitulo ),
                ],
              ),
            ),

            Icon( Icons.star, color: Colors.red, size: 30.0 ),
             Icon( Icons.star, color: Colors.red, size: 30.0 ),
              Icon( Icons.star, color: Colors.red, size: 30.0 ),
               Icon( Icons.star, color: Colors.red, size: 30.0 ),
                Icon( Icons.star, color: Colors.red, size: 30.0 ),

          ],
        ),
      ),
    );
  }
 Widget _crearTexto() {

    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric( horizontal: 40.0 ),
        child: Text(
          'El Tumi es un tipo de cuchillo ceremonial usado en el Antiguo Perú por la cultura Sicán. Habitualmente está formado por una sola pieza metálica. El mango de un tumi tiene forma rectangular o trapezoidal. Aunque su longitud es variable, esta siempre excede el ancho de una mano. En uno de los extremos del mango está la marca característica de los tumis: una hoja cortante en forma semicircular (donde el lado curvo es el que tiene el filo y el lado recto es perpendicular al mango) ',
          textAlign: TextAlign.justify,
        ),
      ),
    );

  }


  Widget _crearActoresPageView( ) {

    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        pageSnapping: false,
        controller: PageController(
          viewportFraction: 0.3,
          initialPage: 1
        ),
        itemCount: 3,
        itemBuilder: (context, i) =>_hotelTarjeta(),
      ),
    );

  }

  Widget _hotelTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'http://2.bp.blogspot.com/-DHeCSVB4NeU/Thx8YnkH1kI/AAAAAAAAAJk/9EINEiHN8QM/s320/LA+CERAMICA+CHIMU++1.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Historia',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
  Widget _artesaniaTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://1.bp.blogspot.com/_uheNlUAGBA8/SmIxW85JelI/AAAAAAAABdM/ymmU10xzZ_c/s320/sipan3.jpg'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Cultura',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _restauranteTarjeta(  ) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal:5.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            
            borderRadius: BorderRadius.circular(10.0),
            child: FadeInImage(
              image: NetworkImage( 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9gQVhpO89KPfUVYeVaXRk6JxmBRzmEr4giw&usqp=CAU'),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              width: 110,
              fit: BoxFit.cover,
            ),
          ),
          Text(
            'Moneda',
            overflow: TextOverflow.ellipsis,
          )
        ],
      )
    );
  }
    Widget _listaTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Preguntas Frecuentes', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.folder_open,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
 Widget _vuelosTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Vuelos', style: TextStyle(color:Colors.deepPurple ) ),
        leading: Icon(Icons.airplanemode_on,color: Colors.deepPurple,)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }
Widget _extraTarjeta( ) {
      return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          ListTile(
        title: Text( 'Recomendaciones', style: TextStyle(color: Colors.deepPurple ) ),
        leading: Icon(Icons.wysiwyg,color: Colors.deepPurple)  ,
        trailing: Icon ( Icons.keyboard_arrow_right, color: Colors.deepPurple ),
        onTap: () {

          //Navigator.pushNamed(context, opt['ruta'] );
       },
      ),


        
        ]
            ),
        );
    }


}
