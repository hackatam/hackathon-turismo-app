import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:tflite/tflite.dart';
import 'dart:math' as math;
import 'package:url_launcher/url_launcher.dart';

import 'camera.dart';
import 'bndbox.dart';
import 'models.dart';

class ScanPage extends StatefulWidget {
  final List<CameraDescription> cameras;

  ScanPage(this.cameras);

  @override
  _ScanPageState createState() => new _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  String _model = "";
  var valor1="Valor 1" ;
  var valor2="Valor 1" ;
  var valortur="Valor Tur" ;
  var valorweb;

  @override
  void initState() {
    super.initState();
  }

  loadModel() async {
    String res;
    switch (_model) {
      case yolo:
        res = await Tflite.loadModel(
          model: "assets/yolov2_tiny.tflite",
          labels: "assets/yolov2_tiny.txt",
        );
        break;

      case mobilenet:
        res = await Tflite.loadModel(
            model: "assets/model_unquant.tflite",
            labels: "assets/labels.txt");

            /*model: "assets/mobilenet_v1_1.0_224.tflite",
            labels: "assets/mobilenet_v1_1.0_224.txt");*/
        break;

      case posenet:
        res = await Tflite.loadModel(
            model: "assets/posenet_mv1_075_float_from_checkpoints.tflite");
        break;

      default:
        res = await Tflite.loadModel(
            model: "assets/ssd_mobilenet.tflite",
            labels: "assets/ssd_mobilenet.txt");
    }
    print(res);
  }

  onSelect(model) {
    setState(() {
      _model = model;
    });
    loadModel();
  }

  setRecognitions(recognitions, imageHeight, imageWidth) {
    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('IA-Yolo-Flutter \'A tu alcance Perú\''),
        centerTitle: true,
      ),
      body: _model == ""
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    color: Colors.red,
                    textColor:Colors.white ,
                    shape: StadiumBorder(),
                    child: const Text(ssd),
                    onPressed: () => onSelect(ssd),
                  ),
                  RaisedButton(
                    color: Colors.red,
                    textColor:Colors.white ,
                    shape: StadiumBorder(),
                    child: const Text(yolo),
                    onPressed: () => onSelect(yolo),
                  ),
                  RaisedButton(
                    color: Colors.red,
                    textColor:Colors.white ,
                    shape: StadiumBorder(),
                    child: const Text(mobilenet),
                    onPressed: () => onSelect(mobilenet),
                  ),
                  RaisedButton(                    
                    color: Colors.red,
                    textColor:Colors.white ,
                    shape: StadiumBorder(),
                    child: const Text(posenet),
                    onPressed: () => onSelect(posenet),
                  ),
                ],
              ),
            )
          : Stack(
              children: [
                Camera(
                  widget.cameras,
                  _model,
                  setRecognitions,
                ),
                BndBox(
                    _recognitions == null ? [] : _recognitions,
                    math.max(_imageHeight, _imageWidth),
                    math.min(_imageHeight, _imageWidth),
                    screen.height,
                    screen.width,
                    _model),

                Center(
                      
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children:<Widget> [
                         Container(
                          child: RaisedButton(
                            child: Text('$valor1'),
                            color: Colors.red,
                            textColor: Colors.white,
                            shape: StadiumBorder(),

                            onPressed: (){
                              List<dynamic> valor=_recognitions.map((re) => re["detectedClass"]).toList();                            
                              valor1=valor[0];
                              setState(() {



                              });   

                            },

                            
                            ),
                            

                            
                            
                            
                        ),
                        Container(
                          height: 15,
                        ),
                         Container(
                          child: RaisedButton(
                            child: Text('$valortur'),
                            color: Colors.red,
                            textColor: Colors.white,
                            
                            shape: StadiumBorder(),

                            onPressed: (){
                              List<dynamic> valorturirsmo=_recognitions.map((re) => re["label"]).toList();                            
                              valortur=valorturirsmo[0];
                              setState(() {



                              });   

                            },

                            
                            ),
                            

                            
                            
                            
                        ),
                         Container(
                          height: 15,
                        ),
                        

                        ],

                      ),
                    )                    
              ],
              
            ),


            floatingActionButton:_crearBotones(),


    );
  }

    Widget _crearBotones(){


    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: 30.0,),
        FloatingActionButton(
          heroTag: "btn1",
          child: Icon(Icons.cancel),
          onPressed: () {
            Navigator.pop(context);
          },

           ),
        Expanded(child: SizedBox()),

        FloatingActionButton(
          heroTag: "btn2",
   
          onPressed: () {
            if(valor1=='dog'){
            _launchURL1();
            }     
          if(valor1=='cat'){
            _launchURL2();
            }      
          if(valor1=='person'){
            _launchURL3();
            } 
          if(valortur=='MachuPicchu'){
            Navigator.pushReplacementNamed(context, 'MP' ) ;
            }     
          if(valortur=='MontanaSieteColores'){
            Navigator.pushReplacementNamed(context, 'MSColores' ) ;
            }      
          if(valortur=='Nasca'){
            Navigator.pushReplacementNamed(context, 'Nasca' ) ;
            }     
          if(valortur=='Tumi'){
            Navigator.pushReplacementNamed(context, 'Tumi' ) ;
            }       
          },



          child: Icon(Icons.web),

           ),


      ],

    );
  }
  
  _launchURL1() async {
  const url = 'https://www.anipedia.net/perros/#:~:text=El%20perro%20pertenece%20a%20la,de%20la%20raza%20de%20perro.&text=Sus%20funciones%20son%20muy%20diversas,el%20mejor%20amigo%20del%20hombre.';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  _launchURL2() async {
  const url = 'https://www.purina.es/gatos/tener-nuevo-gato/buscar-gato-adecuado-para-mi/beneficios-de-tener-un-gato';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  _launchURL3() async {
  const url = 'https://concepto.de/persona-2/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

  _launchURL4() async {
  const url = 'https://www.peru.travel/pe/atractivos/machu-picchu';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  _launchURL5() async {
  const url = 'https://es.wikipedia.org/wiki/Vinicunca';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  _launchURL6() async {
  const url = 'https://www.peru.travel/es/atractivos/lineas-de-nasca';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
}
