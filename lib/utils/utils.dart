import 'package:flutter/material.dart';
import 'package:atualcanceperu/providers/db_provider.dart';
import 'package:url_launcher/url_launcher.dart';

launchURL( BuildContext context, ScanModel scan  ) async {

  final url = scan.valor;

  if ( scan.tipo == 'http' ) {
    if(scan.valor=='http:MachuPichu'){
      Navigator.pushReplacementNamed(context, 'MP' ) ;
    }
    if(scan.valor=='http:Montanasietecolores'){
      Navigator.pushReplacementNamed(context, 'MSColores' ) ;
    }
   if(scan.valor=='http:nasca'){
      Navigator.pushReplacementNamed(context, 'Nasca' ) ;

    }
   if(scan.valor=='http:tumi'){
      Navigator.pushReplacementNamed(context, 'Tumi' ) ;

    }
    // Abrir el sitio web
    /*if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }*/

  } else {
    Navigator.pushNamed(context, 'mapa', arguments: scan );
  }

}