import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:atualcanceperu/widgets/menu_widget.dart';

class Inicio extends StatefulWidget {
  final List<CameraDescription> cameras;

  Inicio(this.cameras);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  String _model = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
        title: Text('IA-Yolo-Flutter2 \'A tu alcance Perú\''),
        centerTitle: true,
      ),
      drawer: MenuWidget(),
          body: Container(
            child: Center(
                  child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FadeInImage(
                      placeholder: AssetImage('assets/giphy.gif'),
                      image: NetworkImage('https://image.freepik.com/vector-gratis/fondo-plano-inteligencia-artificial_23-2147720834.jpg'),
                      fadeInDuration: Duration(milliseconds:200),
                      fit: BoxFit.cover,
                      ),
                      SizedBox(height:30),
                  ],
                ),
            )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        heroTag: "btn1",
        onPressed: () {
          Navigator.pushNamed(context, 'scan',arguments: widget.cameras);
        },
        child: Icon(Icons.double_arrow),
      )
    );
  }
}